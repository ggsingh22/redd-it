# REDD-IT SKILLS ASSIGNMENT  

Browse popular discussions and posts about popular Frontend threads such as JavaScript, React, Web Development, Open Source, Programming and so on. 

![Giphy](https://media.giphy.com/media/4NtPrzG6Wc45V2m2oA/giphy.gif)

Use Reddit JSON API as data source, Redux for state management and React to wrap up all up into a WebApp. Any UI Library added as helpers.  

REDDIT JSON API documentation can be found here - https://github.com/reddit-archive/reddit/wiki/JSON

## STACK TO BE USED : 
   1. HTML5 and CSS3
   1. JS FRAMEWORK(ES6) (REACT JS)
   2. REDUX (For data flow)
   3. WEBPACK (For build process)
   4. GIT

### TASKS TO BE DONE :  
   1.  Create a web application using REDDIT JSON API and show popular Frontend threads such as JavaScript, React, Web Development, Open Source, Programming and others on separate route.
   2.  Show detailed information on the click of popular thread posts of each category( JavaScript, React, Web Development, Open Source etc...).
   3.  Every page should have a search bar to let user search any post or subpost list.
   4.  Use CSS PREPROCESSORS(SASS/LESS/SCSS).
   5.  Use ROBOTO as font and load it using google CDN link and take care that FLASH OF UNSTYLED TEXT(FOUT) issue doesn't happen.
   6.  Try to make design responsive , clean and appealing.
   7.  Encrypt all the network call using algorithms like AES-256-CBC or any other(for the reason that your data is not available from frontend by any means as data is sensitive).
   8.  Optimise build using Webpack (Minify, Uglify, Compress, Code Splitting, Unused code removal, commented code removal etc).
   9.  Use loader when network request is happening.
   10. Use service worker to have control over network request (so that if needed caching can be implemented further in application).
   11. Upload(Maintain) your codebase on github or bitbucket and share the link the final link with us. 
   12. Host your application on any platform (AWS preffered) for live demo.
   13. Try to use any ES6 features only and refrain from mixing ES5.
   14. Modularise your code as much you can.
   15. Use proper commenting to make your code understandable.
   16. Create a short README for this app.

#### Have a nice day and keep clattering your fingers over your keyboards!

   GAURAV SINGH
   FRONTEND DEVELOPER
   (ADCAT, HDFC LIFE)
   bitbucket: [@ggsingh22](https://bitbucket.org/ggsingh22)
   contact on twitter: [@ExtremeRights](https://twitter.com/ExtremeRights)